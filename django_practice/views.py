from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password
from .models import ToDoItem, Event
from .forms import (
    LoginForm,
    AddTaskForm,
    UpdateTaskForm,
    RegisterForm,
    UpdateUserForm,
    AddEventForm,
)


def index(request):
    # todoitem_list = ToDoItem.objects.all()
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    event_list = Event.objects.filter(user_id=request.user.id)
    context = {
        "todoitem_list": todoitem_list,
        "user": request.user,
        "event_list": event_list,
    }
    return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))


def event(request, event_id):
    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    event = get_object_or_404(Event, pk=event_id)
    return render(request, "todolist/event.html", model_to_dict(event))


# def add_task(request):
#     context = {}
#     if request.method == "POST":
#         form = AddTaskForm(request.POST)
#         if form.is_valid() == False:
#             form = AddTaskForm()
#         else:
#             task_name = form.cleaned_data["task_name"]
#             description = form.cleaned_data["description"]
#             duplicates = ToDoItem.objects.filter(task_name=task_name)


#             if not duplicates:
#                 ToDoItem.objects.create(
#                     task_name=task_name,
#                     description=description,
#                     date_created=timezone.now(),
#                     user_id=request.user.id,
#                 )
#                 return redirect("todolist:index")
#             else:
#                 context = {"error": True}
#     return render(request, "todolist/add_task.html", context)
def register(request):
    # users = User.objects.all()
    is_user_registered = False
    context = {"is_user_registered": is_user_registered}
    form = RegisterForm(request.POST)
    if form.is_valid() == False:
        form = RegisterForm()
    else:
        username = form.cleaned_data["username"]
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        email = form.cleaned_data["email"]
        password = make_password(form.cleaned_data["password"])
        duplicates = User.objects.filter(email=email)
        if not duplicates:
            User.objects.create(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
            )
            context = {"first_name": first_name, "last_name": last_name}
            # return redirect("todolist:index")
        else:
            is_user_registered = True

        # first_name = form.cleaned_data["first_name"]

    # for indiv_user in users:
    #     if indiv_user.username == "johndoe":
    #         is_user_registered = True
    #         break
    # if not is_user_registered:
    #     user = User()
    #     user.username = "cat"
    #     user.first_name = "hot"
    #     user.last_name = "dog"
    #     user.email = "hotdog@mail.com"
    #     user.set_password("cathotdog")
    #     user.is_staff = False
    #     user.is_active = True
    #     user.save()
    #     context = {"first_name": user.first_name, "last_name": user.last_name}
    return render(request, "todolist/register.html", context)


def change_password(request):
    try:
        user_object = User.objects.filter(pk=request.user.id)
        user = user_object[0]
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name,
        }

        if request.method == "POST":
            form = UpdateUserForm(request.POST)
            if form.is_valid() == False:
                form = UpdateUserForm()

            else:
                first_name = form.cleaned_data["first_name"]
                last_name = form.cleaned_data["last_name"]
                user.first_name = first_name
                user.last_name = last_name
                print(form.cleaned_data["password"])
                if form.cleaned_data["password"] != "":
                    user.password = make_password(form.cleaned_data["password"])
                user.save()
                return redirect("todolist:index")

    except:
        context = {"error": True}

    return render(request, "todolist/change_password.html", context)


def login_view(request):
    context = {}
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            context = {"username": username, "password": password}
            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {"error": True}
    return render(request, "todolist/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("todolist:index")


def add_task(request):
    context = {}
    if request.method == "POST":
        form = AddTaskForm(request.POST)
        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data["task_name"]
            description = form.cleaned_data["description"]
            duplicates = ToDoItem.objects.filter(item_name=task_name)
            try:
                if not duplicates:
                    ToDoItem.objects.create(
                        item_name=task_name,
                        category=description,
                        date_created=timezone.now(),
                        user=User.objects.get(id=request.user.id),
                    )
                    return redirect("todolist:index")
                else:
                    context = {"error": True}
            except User.DoesNotExist:
                ...

    return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk=todoitem_id)
    item = todoitem[0]
    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        "task_name": item.task_name,
        "description": item.description,
        "status": item.status,
    }

    if request.method == "POST":
        form = UpdateTaskForm(request.POST)
        if form.is_valid() == False:
            form = UpdateTaskForm()

        else:
            task_name = form.cleaned_data["task_name"]
            description = form.cleaned_data["description"]
            status = form.cleaned_data["status"]
            if todoitem:
                item.task_name = task_name
                item.description = description
                item.status = status
                item.save()
                return "todolist:index"
            else:
                context = {"error": True}
    return render(request, "todolist:/update_task.html", context)


def delete_task(request, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
    return redirect("todolist:index")


def add_event(request):
    context = {}
    if request.method == "POST":
        form = AddEventForm(request.POST)
        if form.is_valid() == False:
            form = AddEventForm()
        else:
            event_name = form.cleaned_data["event_name"]
            description = form.cleaned_data["description"]
            duplicates = Event.objects.filter(event_name=event_name)
            try:
                if not duplicates:
                    Event.objects.create(
                        event_name=event_name,
                        description=description,
                        event_date=timezone.now(),
                        user_id=User.objects.get(id=request.user.id),
                    )
                    return redirect("todolist:index")
                else:
                    context = {"error": True}
            except Event.DoesNotExist:
                ...

    return render(request, "todolist/add_event.html", context)
